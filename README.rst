##################
Bastelino ESP32-S2
##################

.. image:: doc/hardware/pcb_preliminary.jpg

Overview
========

The Bastelino ESP32-S2 is a development & experimentation
board with Arduino compatible form factor powered by  
an Espressif ESP32-S2-WROOM module. This module is in turn 
using an ESP32-S2 SoC with the following noteable features:

* 240MHz Tensilica LX7 single core processor
* 2.4 GHz Wifi

Aside that, the board contains a WCH552G IC providing USB serial
for programming and log output  and a jtag debugger using
espjtag emulation provided by the most recent `openocd-esp`_ 
version.

Quick start
===========

Additional requirements
-----------------------

- `esp-idf`_ for development and flashing via USB
- `openocd-esp`_ in a very new version for use of the jtag interface
- eventually `wchprog`_, `wchisptool`_ ore some other clone to flash the WCH552G firmware

.. _esp-idf: https://github.com/espressif/esp-idf
.. _openocd-esp: https://github.com/espressif/openocd-esp32
.. _wchprog: https://github.com/juliuswwj/wchprog
.. _wchisptool: http://www.wch.cn/download/WCHISPTool_Setup_exe.html

Preparing the programmer
------------------------

At first the on-board programmer needs to be flashed with its firmware. For
that purpose, set the jumper JP1 and connect the board to USB port. Because of
the jumper, the WCH552 will enter bootloader mode and can be 
programmed. E.g. using wchprog::

  python2 wchprog.py firmware/programmer/bastellino_prog.hex

Flashing ESP32S2 with idf.py
----------------------------

The ESP32-S2 can be flashed using the standard ``idf.py`` tool from Espressif
but the baudrate must be set to 115200 since the onboard programmer does currently 
not support higher baud rates::

  idf.py -p /dev/ttyACM0 -b 115200 flash


Debuging with openocd
---------------------

**WARNING:** While in principle this should be working, there seems to be issues
with the current implementation. You weel need a very new `openocd-esp`_ in order
to have support for the espjtag driver. The run openocd as usual using the following
interface configuration::

  interface esp_usb_jtag


Status
======

- The Usb serial interface for programming / debug out put is ready to use
- The onboard jtag interface is working regarding the hardware but there
  still seems to be an issue of the interworking between openocd and the
  espjtag emulation.

Licensing
=========

CC BY-SA for all files in ``hardware`` subfolder. For files 
in firmware subfolder the license is defined at the file beginning.

Copyright
=========

For all files in ``hardware`` folder copyright is
(c) 2021 Andreas Messer <andi@bastelmap.de>. For files 
in firmware subfolder the copright is defined at the 
file beginning.

References
==========


.. target-notes::
