*************************
Bastelino ESP32-S2 Errata
*************************

Hardware Revision 1
===================

.. image:: doc/hardware/pcb_preliminary.jpg

1. 10k Pull-Up Resistor is missing at the EN pin of ESP32-S2-WROOM module



