/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "io.h"

#define CDC_DEBUG (DEBUG_ESPJTAG)

enum 
{
  USB_CDC_SET_LINE_CODING        = 0x20,           // Configures DTE rate, stop-bits, parity, and number-of-character
  USB_CDC_GET_LINE_CODING        = 0x21,           // This request allows the host to find out the currently configured line coding.
  USB_CDC_SET_CONTROL_LINE_STATE = 0x22,           // This request generates RS-232/V.24 style control signals.
};

__idata uint8_t UsbCdc_ep_out_datalen[2];
__idata static uint8_t  usb_cdc_inlen[2];

__idata static uint8_t uart_txbuf;
__idata static uint8_t uart_txoffset;

__idata static uint8_t uart_rxbuf;
__idata static uint8_t uart_rxoffset;

__idata static uint8_t uart_interchar_ticks;

__xdata uint8_t UsbCdc_linecoding[7];

#pragma save
#pragma nooverlay

#define USB_CDC_OUT_BUF(idx) ((idx) ? (usb_ep2_out_buf+MAX_PACKET_SIZE) : (usb_ep2_out_buf))
#define USB_CDC_IN_BUF(idx)  ((idx) ? (usb_ep2_in_buf+MAX_PACKET_SIZE)  : (usb_ep2_in_buf))

void
usb_cdc_out_transfer()
{
  uint8_t reg;
  uint8_t buffer_no;
  
  reg = UEP3_CTRL;

  if (reg & bUEP_R_TOG)
    buffer_no = 1;
  else
    buffer_no = 0;

  UsbCdc_ep_out_datalen[buffer_no]  = USB_RX_LEN;

  /* when other buffer is empty, we should start transmission now */
  if(UsbCdc_ep_out_datalen[1-buffer_no] == 0)
  {
    uart_txbuf    = buffer_no;
    uart_txoffset = 1;
    SBUF          = USB_CDC_OUT_BUF(uart_txbuf)[0];
  }
  else
  {
    reg |= UEP_R_RES_NAK;
  }

  UEP3_CTRL = reg ^ bUEP_R_TOG;
}

void
usb_cdc_in_transfer()
{
  uint8_t reg;
  uint8_t buffer_no;
  
  reg = UEP3_CTRL;

  if (reg & bUEP_T_TOG)
    buffer_no = 1;
  else
    buffer_no = 0;

  usb_cdc_inlen[buffer_no] = 0;

  /* if next buffer does have data, we can continue sending */
  if(usb_cdc_inlen[1-buffer_no] > 0)
  {
    UEP3_T_LEN = usb_cdc_inlen[1-buffer_no];
  }
  else
  {
    UEP3_T_LEN = 0;
    reg |= UEP_T_RES_NAK;
  }

  UEP3_CTRL = reg ^ bUEP_T_TOG;
}

void 
UsbCdc_SysTick()
{
  if(uart_interchar_ticks == 0)
  { /* When inter-character timeout is hit and the transceiver is idle,
     * fire transmission of the data to host */
    uint8_t reg;
    
    reg = UEP3_CTRL;
    if((uart_rxoffset > 0) && 
       (reg & MASK_UEP_T_RES) == UEP_T_RES_NAK)
    {
      usb_cdc_inlen[uart_rxbuf] = uart_rxoffset;
      uart_rxoffset = 0;

      UEP3_T_LEN = usb_cdc_inlen[uart_rxbuf];
      UEP3_CTRL  = reg & (~UEP_T_RES_NAK);

      if(reg & bUEP_T_TOG)
        uart_rxbuf = 0;
      else
        uart_rxbuf = 1;
    }
  }
  else
  {
    --uart_interchar_ticks;
  }
}


static 
void UsbCdc_SetLineCoding(__xdata const uint8_t *linecoding)
{
  uint32_t baudrate = ((uint32_t) linecoding[0])      |
                      ((uint32_t) linecoding[1] << 8) |
                      ((uint32_t) linecoding[2] << 16) |
                      ((uint32_t) linecoding[3] << 24);

  if(System_SetBaudrate(baudrate))
  {
    memcpy(UsbCdc_linecoding, linecoding, sizeof(UsbCdc_linecoding));

    /* NEED to set bits on its own! Otherwise  might accidentally clear
     * interrupt flags */
    SM1 = 1;
    REN = 1;
  }
}

static 
void UsbCdc_SetControlLineState(const __xdata USB_SETUP_REQ* setup_req)
{
#if !USE_UART_SIGNALS
  /* dtr & rts are used to control the io0 and en lines on esp32 boards */
  switch(setup_req->wValueL)
  {
  case 0: /* DTR and RTS deasserted (both high) */
    ESP32_IO0 = 1; /* Boot to firmware */
    nESP32_EN = 0; /* Enable ESP32 */
    break;
  case 1: /* DTR asserted (low) and RTS deasserted (high) */
    ESP32_IO0 = 0; /* Boot to bootloader */
    nESP32_EN = 0; /* Enable ESP32 */
    break;
  case 2: /* DTR deasserted (high) and RTS asserted  (low)*/
    ESP32_IO0 = 1; /* Boot to firmware */
    nESP32_EN = 1; /* Reset ESP 32 */
    break;
  case 3: /* DTR and RTS asserted  both low */
    ESP32_IO0 = 1; /* Boot to firmware */
    nESP32_EN = 0; /* Enable ESP32 */
    break;
  }
#else
  bool dtr_asserted = setup_req->wValueL & 0x01;
  bool rts_asserted = setup_req->wValueL & 0x02;
#endif
}


void
UsbCdc_HandleControlSetup(__idata struct control_transfer_state *transaction)
{
  const __xdata USB_SETUP_REQ* setup_req = (const __xdata USB_SETUP_REQ*) transaction->setup_out_buffer;

  switch( transaction->request )
  {
  case USB_CDC_GET_LINE_CODING:
      transaction->len = sizeof(UsbCdc_linecoding);
      memcpy(transaction->setup_out_buffer, UsbCdc_linecoding, transaction->len);
      break;
  case USB_CDC_SET_LINE_CODING:
      transaction->len = 0;
      break;
  case USB_CDC_SET_CONTROL_LINE_STATE:
      UsbCdc_SetControlLineState(setup_req);
      transaction->len = 0;
      break;
  default:
      transaction->len = -1;
      break;
  }
}

void
UsbCdc_HandleControlOut(__idata struct control_transfer_state *transaction)
{
  switch( transaction->request )
  {
  case USB_CDC_SET_LINE_CODING:
    if(transaction->len == 7)
    {
      UsbCdc_SetLineCoding(transaction->setup_out_buffer);
      transaction->len = 0;
    }
    else
    {
      transaction->len = -1;
    }
    break;
  default:
    transaction->len = -1;
    break;
  }
}

#pragma restore


void 
UsbCdc_Poll()
{
  uint8_t reg;

  /* when USB transmitter is idle, its time to adjust the next
   * used UART receive buffer such that it points to next used
   * buffer */
  reg = UEP3_CTRL;
  if((reg & MASK_UEP_T_RES) == UEP_T_RES_NAK)
  {
    if(reg & bUEP_T_TOG)
      uart_rxbuf = 1;
    else
      uart_rxbuf = 0;
  }
}


void Uart0Isr(void) __interrupt (INT_NO_UART0)
{
  if(RI)
  {
    RI = 0;
#if !CDC_DEBUG
    if(usb_cdc_inlen[uart_rxbuf] == 0)
    {
      USB_CDC_IN_BUF(uart_rxbuf)[uart_rxoffset] = SBUF;
      uart_rxoffset++;

      if(uart_rxoffset >= 64)
      { /* current receive buffer filled up */
        uint8_t reg;
        usb_cdc_inlen[uart_rxbuf] = uart_rxoffset;

        reg = UEP3_CTRL;
        if((reg & MASK_UEP_T_RES) == UEP_T_RES_NAK)
        {
          UEP3_T_LEN = uart_rxoffset;
          UEP3_CTRL  = (reg & ~UEP_T_RES_NAK);
        }

        uart_rxbuf    = 1 - uart_rxbuf;
        uart_rxoffset = 0;

      }
      else
      { /* reset inter char timer */
        uart_interchar_ticks = 2;
      }      
    }
#endif
  }

  if(TI)
  {
    TI = 0;
    if((uart_txoffset > 0) && 
       (uart_txoffset >= UsbCdc_ep_out_datalen[uart_txbuf]))
    { /* last character transmitted */
      uint8_t reg;

      /* release the buffer */
      UsbCdc_ep_out_datalen[uart_txbuf] = 0;
      /* switch to other buffer */
      uart_txbuf    = 1 - uart_txbuf;
      uart_txoffset = 0;

      /* re-enable usb receiver */
      reg = UEP3_CTRL;
      if((reg & MASK_UEP_R_RES) == UEP_R_RES_NAK)
        UEP3_CTRL = reg & (~UEP_R_RES_NAK);
    }

    if(uart_txoffset < UsbCdc_ep_out_datalen[uart_txbuf])
    {
      SBUF = USB_CDC_OUT_BUF(uart_txbuf)[uart_txoffset];
      uart_txoffset++;
    }
  }
}

#if CDC_DEBUG
__code const char kTRACE_ESPJTAG_HEXChars[] = "0123456789abcdef";
__idata uint8_t   cdc_debug_sequence;

static uint8_t 
UsbCdc_WaitMsg()
{
  uint8_t msg_buffer;

  do {
    msg_buffer = usb_cdc_get_next_uart_rxbuffer();
  } while (usb_cdc_inlen[msg_buffer] != 0);

  return msg_buffer;
}

void 
UsbCdc_SendMsg(__code const char* msg, uint16_t val)
{
  uint8_t msg_buffer = UsbCdc_WaitMsg();
  __xdata uint8_t* p = USB_CDC_IN_BUF(msg_buffer);

  *(p++) = kTRACE_ESPJTAG_HEXChars[(++cdc_debug_sequence) & 0xF];
  *(p++) = ' '; 

  while(*msg != 0)
  {
    *(p++) = (*msg++);
  }

  *(p++) = ' '; 
  *(p++) = '0'; 
  *(p++) = 'x';

  *(p++) = kTRACE_ESPJTAG_HEXChars[(val >> 12) & 0xF];
  *(p++) = kTRACE_ESPJTAG_HEXChars[(val >>  8) & 0xF];
  *(p++) = kTRACE_ESPJTAG_HEXChars[(val >>  4) & 0xF];
  *(p++) = kTRACE_ESPJTAG_HEXChars[(val >>  0) & 0xF];
  *(p++) = '\r';
  *(p++) = '\n';

  usb_cdc_inlen[msg_buffer] = p - USB_CDC_IN_BUF(msg_buffer);
  UsbCdc_CheckEnableIn();
}

void 
UsbCdc_SendHex(__code const char* msg, __xdata const uint8_t *data, uint8_t len)
{
  uint8_t msg_buffer = UsbCdc_WaitMsg();

  __xdata uint8_t* p = USB_CDC_IN_BUF(msg_buffer);
  __xdata uint8_t* e = p + MAX_PACKET_SIZE - 4;


  *(p++) = kTRACE_ESPJTAG_HEXChars[(++cdc_debug_sequence) & 0xF];
  *(p++) = ' '; 

  while(*msg != 0)
    *(p++) = (*msg++);

  *(p++) = ' '; 

  while((p < e) && (len > 0))
  {
    *(p++) = kTRACE_ESPJTAG_HEXChars[(*data >> 4) & 0xF];
    *(p++) = kTRACE_ESPJTAG_HEXChars[(*data >> 0) & 0xF];
    ++data;
    --len;
  }

  *(p++) = '\r';
  *(p++) = '\n';

  usb_cdc_inlen[msg_buffer] = p - USB_CDC_IN_BUF(msg_buffer);
  UsbCdc_CheckEnableIn();
}
#endif