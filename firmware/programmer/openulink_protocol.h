/***************************************************************************
 *   Copyright (C) 2011 Martin Schmoelzer <martin.schmoelzer@student.tuwien.ac.at>: Original implementation *
 *   Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>: Modifications & adaptions for bastelino         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/
#ifndef BASTELINO_PROGRAMMER_OPENULINK_PROTOCOL_H_
#define BASTELINO_PROGRAMMER_OPENULINK_PROTOCOL_H_

#include "common.h"
#include <stdbool.h>

__bit extern JTAG_active;

void PollJTAG(void);


#endif
