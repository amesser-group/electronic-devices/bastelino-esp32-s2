/************************************************************************************************************
 *   Copyright (C) 2011 Martin Schmoelzer <martin.schmoelzer@student.tuwien.ac.at>: Original implementation *
 *   Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>: Modifications & adaptions for bastelino         *
 *                                                                                                          *
 *   This program is free software; you can redistribute it and/or modify                                   *
 *   it under the terms of the GNU General Public License as published by                                   *
 *   the Free Software Foundation; either version 2 of the License, or                                      *
 *   (at your option) any later version.                                                                    *
 *                                                                                                          *
 *   This program is distributed in the hope that it will be useful,                                        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of                                         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                          *
 *   GNU General Public License for more details.                                                           *
 *                                                                                                          *
 *   You should have received a copy of the GNU General Public License                                      *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.                                  *
 ************************************************************************************************************/
#include "common.h"
#include "io.h"
#include "openulink_jtag.h"
#include "openulink_msgtypes.h"
#include "openulink_protocol.h"

/**
 * @file
 * Implementation of the OpenULINK communication protocol.
 *
 * The OpenULINK protocol uses one OUT and one IN endpoint. These two endpoints
 * are configured to use the maximum packet size for full-speed transfers,
 * 64 bytes. Commands always start with a command ID (see msgtypes.h for
 * command ID definitions) and contain zero or more payload data bytes in both
 * transfer directions (IN and OUT). The payload
 *
 * Almost all commands contain a fixed number of payload data bytes. The number
 * of payload data bytes for the IN and OUT direction does not need to be the
 * same.
 *
 * Multiple commands may be sent in one EP2 Bulk-OUT packet. Because the
 * OpenULINK firmware does not perform bounds checking for EP2 Bulk-IN packets,
 * the host MUST ensure that the commands sent in the OUT packet require a
 * maximum of 64 bytes of IN data.
 */

__bit JTAG_active;

/** Index in EP2 Bulk-OUT data buffer that contains the current command ID */
__idata uint8_t cmd_id_index;

/** Number of data bytes already in EP2 Bulk-IN buffer */
__idata uint8_t payload_index_in;

/** internal counter for millisecond delay */
__idata uint16_t JTAG_ms_delay;

/** Used to generate a delay */
void
JTAG_TimerIsr(void) __interrupt (INT_NO_TMR1)
{
  TH1 = System_timer_reload_1ms_div12 / 256;
  TL1 = System_timer_reload_1ms_div12 % 256;

  if( JTAG_ms_delay > 1)
  {
    --JTAG_ms_delay;
  }
  else
  {
    JTAG_ms_delay = 0;
    TR1 = 0;
  }
}

void
JTAG_StartTimer(uint16_t count)
{
  TH1 = count / 256;
  TL1 = count % 256;
  TR1 = 1;
}

static void
JTAG_StartMicrosecondDelay(uint16_t us)
{
  uint16_t cnt = 0;

  switch(CLOCK_CFG & MASK_SYS_CK_SEL)
  {
  case SYS_CK_SEL_187k5HZ: cnt = us / 64;       break;
  case SYS_CK_SEL_750KHZ:  cnt = us / 16;       break;
  case SYS_CK_SEL_3MHZ:    cnt = us / 4;        break;
  case SYS_CK_SEL_6MHZ:    cnt = us / 2;        break;
  case SYS_CK_SEL_12MHZ:   cnt = us;            break;
  case SYS_CK_SEL_16MHZ:   cnt = us + us/3;     break;
  case SYS_CK_SEL_24MHZ:   cnt = us * 2;        break;
  case SYS_CK_SEL_32MHZ:   cnt = us * 3 - us/3; break;
  }

  if (cnt > 0)
  {
    JTAG_ms_delay = 1;
    JTAG_StartTimer(0U - cnt);
  }
}

static void
JTAG_StartMillisecondDelay(uint16_t ms)
{
  JTAG_ms_delay = ms;
  JTAG_StartTimer(System_timer_reload_1ms_div12);
}


/**
 * Executes one command and updates global command indexes.
 *
 * @return true if this command was the last command.
 * @return false if there are more commands within the current contents of the
 * Bulk EP2-OUT data buffer.
 */
bool execute_jtag_command(void)
{
  uint8_t usb_out_bytecount, usb_in_bytecount;
  uint16_t signal_state;
  uint16_t count;

  /* Most commands do not transfer IN data. To save code space, we write 0 to
    * usb_in_bytecount here, then modify it in the switch statement below where
    * neccessary */
  usb_in_bytecount = 0;

  switch (usb_ep_out_jtag[cmd_id_index] /* Command ID */) 
  {
      case CMD_SCAN_IN:
      case CMD_SLOW_SCAN_IN:
        usb_out_bytecount = 5;
        usb_in_bytecount = usb_ep_out_jtag[cmd_id_index + 1];
        jtag_scan_in(cmd_id_index + 1, payload_index_in);
        break;
      case CMD_SCAN_OUT:
      case CMD_SLOW_SCAN_OUT:
        usb_out_bytecount = usb_ep_out_jtag[cmd_id_index + 1] + 5;
        jtag_scan_out(cmd_id_index + 1);
        break;
      case CMD_SCAN_IO:
      case CMD_SLOW_SCAN_IO:
        usb_in_bytecount = usb_ep_out_jtag[cmd_id_index + 1];
        usb_out_bytecount = usb_in_bytecount + 5;
        jtag_scan_io(cmd_id_index + 1, payload_index_in);
        break;
      case CMD_CLOCK_TMS:
      case CMD_SLOW_CLOCK_TMS:
        usb_out_bytecount = 2;
        jtag_clock_tms(usb_ep_out_jtag[cmd_id_index + 1], usb_ep_out_jtag[cmd_id_index + 2]);
        break;
      case CMD_CLOCK_TCK:
      case CMD_SLOW_CLOCK_TCK:
        usb_out_bytecount = 2;
        count = (uint16_t)usb_ep_out_jtag[cmd_id_index + 1];
        count |= ((uint16_t)usb_ep_out_jtag[cmd_id_index + 2]) << 8;
        jtag_clock_tck(count);
        break;
      case CMD_SLEEP_US:
        usb_out_bytecount = 2;
        count = (uint16_t)usb_ep_out_jtag[cmd_id_index + 1];
        count |= ((uint16_t)usb_ep_out_jtag[cmd_id_index + 2]) << 8;
        JTAG_StartMicrosecondDelay(count);
        break;
      case CMD_SLEEP_MS:
        usb_out_bytecount = 2;
        count  = (uint16_t)usb_ep_out_jtag[cmd_id_index + 1];
        count |= ((uint16_t)usb_ep_out_jtag[cmd_id_index + 2]) << 8;
        JTAG_StartMillisecondDelay(count);
        break;
      case CMD_GET_SIGNALS:
        usb_out_bytecount = 0;
        usb_in_bytecount = 2;
        signal_state = jtag_get_signals();
        usb_ep_in_jtag[payload_index_in] = (signal_state >> 8) & 0x00FF;
        usb_ep_in_jtag[payload_index_in + 1] = signal_state & 0x00FF;
        break;
      case CMD_SET_SIGNALS:
        usb_out_bytecount = 2;
        jtag_set_signals(usb_ep_out_jtag[cmd_id_index + 1], usb_ep_out_jtag[cmd_id_index + 2]);
        /* insert artifical delay after any pin change */
        JTAG_StartMillisecondDelay(100);
        break;
      case CMD_CONFIGURE_TCK_FREQ:
        usb_out_bytecount = 5;
        break;
      case CMD_SET_LEDS:
        usb_out_bytecount = 1;
        break;
      case CMD_TEST:
        JTAG_active = 1;
        System_UpdateClock();
        usb_out_bytecount = 1;
        break;
      default:
        usb_out_bytecount = 0;
        break;
  }

  payload_index_in += usb_in_bytecount;
  cmd_id_index += (usb_out_bytecount + 1);

  if (cmd_id_index >= usb_ep_out_jtag_bytecount)
    return true;
  else
    return false;
}

/**
 * Forever wait for commands and execute them as they arrive.
 */
void PollJTAG(void)
{
  /* only do anything if endpoint receive is disabled */
  if((UEP2_CTRL & MASK_UEP_R_RES) != UEP_R_RES_NAK)
    return;

  /* when TR1 is running, we're waiting on a delay */
  if(TR1)
    return;

  if (cmd_id_index < usb_ep_out_jtag_bytecount)
  { /* there are commands to process */
    bool last_command = execute_jtag_command();

    if (last_command)
    {
      usb_ep_out_jtag_bytecount = 0;
      cmd_id_index              = 0;

      /* Send back EP2 Bulk-IN packet if required */
      if(payload_index_in > 0)
      {
          UEP2_T_LEN = payload_index_in;
          payload_index_in = 0;

          UEP2_CTRL &= ~UEP_T_RES_NAK;
      }
    }
  }
  else
  {
    /* in buffer transmitted to host, re-enable receiver */
    if((UEP2_CTRL & MASK_UEP_T_RES) == UEP_T_RES_NAK)
      UEP2_CTRL &= ~UEP_R_RES_NAK;
  }
}
