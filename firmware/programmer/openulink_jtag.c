/************************************************************************************************************
 *   Copyright (C) 2011 Martin Schmoelzer <martin.schmoelzer@student.tuwien.ac.at>: Original implementation *
 *   Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>: Modifications & adaptions for bastelino         *
 *                                                                                                          *
 *   This program is free software; you can redistribute it and/or modify                                   *
 *   it under the terms of the GNU General Public License as published by                                   *
 *   the Free Software Foundation; either version 2 of the License, or                                      *
 *   (at your option) any later version.                                                                    *
 *                                                                                                          *
 *   This program is distributed in the hope that it will be useful,                                        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of                                         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                          *
 *   GNU General Public License for more details.                                                           *
 *                                                                                                          *
 *   You should have received a copy of the GNU General Public License                                      *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.                                  *
 ************************************************************************************************************/
#include "common.h"
#include "io.h"
#include "openulink_jtag.h"
#include "openulink_msgtypes.h"

/**
 * Perform JTAG SCAN-IN operation at maximum TCK frequency.
 *
 * Dummy data is shifted into the JTAG chain via TDI, TDO data is sampled and
 * stored in the EP2 IN buffer.
 *
 * Maximum achievable TCK frequency is 182 kHz for ULINK clocked at 24 MHz.
 *
 * @param out_offset offset in usb_ep_out_jtag where payload data starts
 */
void jtag_scan_in(uint8_t out_offset, uint8_t in_offset)
{
  uint8_t scan_size_bytes, bits_last_byte;
  uint8_t tms_count_start, tms_count_end;
  uint8_t tms_sequence_start, tms_sequence_end;
  uint8_t tdo_data, i, j;

  /* Get parameters from usb_ep_out_jtag */
  scan_size_bytes = usb_ep_out_jtag[out_offset];
  bits_last_byte = usb_ep_out_jtag[out_offset + 1];
  tms_count_start = (usb_ep_out_jtag[out_offset + 2] >> 4) & 0x0F;
  tms_count_end = usb_ep_out_jtag[out_offset + 2] & 0x0F;
  tms_sequence_start = usb_ep_out_jtag[out_offset + 3];
  tms_sequence_end = usb_ep_out_jtag[out_offset + 4];

  if (tms_count_start > 0)
      jtag_clock_tms(tms_count_start, tms_sequence_start);

  ESP32_TDI = 0;
  ESP32_TMS = 0;

  /* Shift all bytes except the last byte */
  for (i = 0; i < scan_size_bytes - 1; i++)
  {
    tdo_data = 0;

    for (j = 0; j < 8; j++)
    {
      ESP32_TCK = 0;
      tdo_data = tdo_data >> 1;
      ESP32_TCK = 1;

      if (ESP32_TDO)
        tdo_data |= 0x80;
    }

    /* Copy TDO data to usb_ep_in_jtag */
    usb_ep_in_jtag[i + in_offset] = tdo_data;
  }

  tdo_data = 0;

  /* Shift the last byte */
  for (j = 0; j < bits_last_byte; j++)
  {
    /* Assert TMS signal if requested and this is the last bit */
    if ((j == bits_last_byte - 1) && (tms_count_end > 0))
    {
      ESP32_TMS = 1;
      tms_count_end--;
      tms_sequence_end = tms_sequence_end >> 1;
    }

    ESP32_TCK = 0;
    tdo_data = tdo_data >> 1;
    ESP32_TCK = 1;

    if (ESP32_TDO)
        tdo_data |= 0x80;
  }

  tdo_data = tdo_data >> (8 - bits_last_byte);

  /* Copy TDO data to usb_ep_in_jtag */
  usb_ep_in_jtag[i + in_offset] = tdo_data;

  /* Move to correct end state */
  if (tms_count_end > 0)
    jtag_clock_tms(tms_count_end, tms_sequence_end);
}


/**
 * Perform JTAG SCAN-OUT operation at maximum TCK frequency.
 *
 * Data stored in EP2 OUT buffer is shifted into the JTAG chain via TDI, TDO
 * data is not sampled.
 * The TAP-FSM state is alyways left in the PAUSE-DR/PAUSE-IR state.
 *
 * Maximum achievable TCK frequency is 142 kHz for ULINK clocked at 24 MHz.
 *
 * @param out_offset offset in usb_ep_out_jtag where payload data starts
 */
void jtag_scan_out(uint8_t out_offset)
{
  uint8_t scan_size_bytes, bits_last_byte;
  uint8_t tms_count_start, tms_count_end;
  uint8_t tms_sequence_start, tms_sequence_end;
  uint8_t tdi_data, i, j;

  /* Get parameters from usb_ep_out_jtag */
  scan_size_bytes = usb_ep_out_jtag[out_offset];
  bits_last_byte = usb_ep_out_jtag[out_offset + 1];
  tms_count_start = (usb_ep_out_jtag[out_offset + 2] >> 4) & 0x0F;
  tms_count_end = usb_ep_out_jtag[out_offset + 2] & 0x0F;
  tms_sequence_start = usb_ep_out_jtag[out_offset + 3];
  tms_sequence_end = usb_ep_out_jtag[out_offset + 4];

  if (tms_count_start > 0)
      jtag_clock_tms(tms_count_start, tms_sequence_start);

  ESP32_TMS = 0;

  /* Shift all bytes except the last byte */
  for (i = 0; i < scan_size_bytes - 1; i++)
  {
    tdi_data = usb_ep_out_jtag[i + out_offset + 5];

    for (j = 0; j < 8; j++)
    {
      if (tdi_data & 0x01)
        ESP32_TDI = 1;
      else
        ESP32_TDI = 0;

      ESP32_TCK = 0;
      tdi_data  = tdi_data >> 1;
      ESP32_TCK = 1;
    }
  }

  tdi_data = usb_ep_out_jtag[i + out_offset + 5];

  /* Shift the last byte */
  for (j = 0; j < bits_last_byte; j++)
  {
    if (tdi_data & 0x01)
        ESP32_TDI = 1;
    else
        ESP32_TDI = 0;

    /* Assert TMS signal if requested and this is the last bit */
    if ((j == bits_last_byte - 1) && (tms_count_end > 0))
    {
      ESP32_TMS = 1;
      tms_count_end--;
      tms_sequence_end = tms_sequence_end >> 1;
    }

    ESP32_TCK = 0;
    tdi_data = tdi_data >> 1;
    ESP32_TCK = 1;
  }

  /* Move to correct end state */
  if (tms_count_end > 0)
    jtag_clock_tms(tms_count_end, tms_sequence_end);
}


/**
 * Perform bidirectional JTAG SCAN operation at maximum TCK frequency.
 *
 * Data stored in EP2 OUT buffer is shifted into the JTAG chain via TDI, TDO
 * data is sampled and stored in the EP2 IN buffer.
 * The TAP-FSM state is alyways left in the PAUSE-DR/PAUSE-IR state.
 *
 * Maximum achievable TCK frequency is 100 kHz for ULINK clocked at 24 MHz.
 *
 * @param out_offset offset in usb_ep_out_jtag where payload data starts
 */
void jtag_scan_io(uint8_t out_offset, uint8_t in_offset)
{
  uint8_t scan_size_bytes, bits_last_byte;
  uint8_t tms_count_start, tms_count_end;
  uint8_t tms_sequence_start, tms_sequence_end;
  uint8_t tdi_data, tdo_data, i, j;

  /* Get parameters from usb_ep_out_jtag */
  scan_size_bytes = usb_ep_out_jtag[out_offset];
  bits_last_byte = usb_ep_out_jtag[out_offset + 1];
  tms_count_start = (usb_ep_out_jtag[out_offset + 2] >> 4) & 0x0F;
  tms_count_end = usb_ep_out_jtag[out_offset + 2] & 0x0F;
  tms_sequence_start = usb_ep_out_jtag[out_offset + 3];
  tms_sequence_end = usb_ep_out_jtag[out_offset + 4];

  if (tms_count_start > 0)
    jtag_clock_tms(tms_count_start, tms_sequence_start);

  ESP32_TMS = 0;

  /* Shift all bytes except the last byte */
  for (i = 0; i < scan_size_bytes - 1; i++)
  {
    tdi_data = usb_ep_out_jtag[i + out_offset + 5];
    tdo_data = 0;

    for (j = 0; j < 8; j++)
    {
      if (tdi_data & 0x01)
        ESP32_TDI = 1;
      else
        ESP32_TDI = 0;

      ESP32_TCK = 0;
      tdi_data = tdi_data >> 1;
      ESP32_TCK = 1;
      tdo_data = tdo_data >> 1;

      if (ESP32_TDO)
          tdo_data |= 0x80;
    }

    /* Copy TDO data to usb_ep_in_jtag */
    usb_ep_in_jtag[i + in_offset] = tdo_data;
  }

  tdi_data = usb_ep_out_jtag[i + out_offset + 5];
  tdo_data = 0;

    /* Shift the last byte */
  for (j = 0; j < bits_last_byte; j++)
  {
    if (tdi_data & 0x01)
      ESP32_TDI = 1;
    else
      ESP32_TDI = 0;

    /* Assert TMS signal if requested and this is the last bit */
    if ((j == bits_last_byte - 1) && (tms_count_end > 0))
    {
      ESP32_TMS = 1;
      tms_count_end--;
      tms_sequence_end = tms_sequence_end >> 1;
    }

    ESP32_TCK = 0;
    tdi_data = tdi_data >> 1;
    ESP32_TCK = 1;
    tdo_data = tdo_data >> 1;

    if (ESP32_TDO)
      tdo_data |= 0x80;
  }
  tdo_data = tdo_data >> (8 - bits_last_byte);

  /* Copy TDO data to usb_ep_in_jtag */
  usb_ep_in_jtag[i + in_offset] = tdo_data;

  /* Move to correct end state */
  if (tms_count_end > 0)
    jtag_clock_tms(tms_count_end, tms_sequence_end);
}


/**
 * Generate TCK clock cycles.
 *
 * Maximum achievable TCK frequency is 375 kHz for ULINK clocked at 24 MHz.
 *
 * @param count number of TCK clock cyclces to generate.
 */
void jtag_clock_tck(uint16_t count)
{
  while(count > 0)
  {
    ESP32_TCK = 0;
    --count;
    ESP32_TCK = 1;
  }
}

/**
 * Perform TAP FSM state transitions at maximum TCK frequency.
 *
 * Maximum achievable TCK frequency is 176 kHz for ULINK clocked at 24 MHz.
 *
 * @param count the number of state transitions to perform.
 * @param sequence the TMS pin levels for each state transition, starting with
 *  the least-significant bit.
 */
void jtag_clock_tms(uint8_t count, uint8_t sequence)
{
    while(count > 0)
  {
    /* Set TMS pin according to sequence parameter */
    if (sequence & 0x1)
      ESP32_TMS = 1;
    else
      ESP32_TMS = 0;

    ESP32_TCK = 0;
    sequence = sequence >> 1;
    ESP32_TCK = 1;
    --count;
  }
}

/**
 * Get current JTAG signal states.
 *
 * @return a 16-bit integer where the most-significant byte contains the state
 *  of the JTAG input signals and the least-significant byte contains the state
 *  of the JTAG output signals.
 */
uint16_t jtag_get_signals(void)
{
  uint8_t input_signal_state, output_signal_state;

  input_signal_state  = 0;
  output_signal_state = SIGNAL_TRST;

  /* Get states of input pins */
  if (ESP32_TDO)
    input_signal_state |= SIGNAL_TDO;

  if(ESP32_TDI)
    output_signal_state |= SIGNAL_TDI;

  if(ESP32_TMS)
    output_signal_state |= SIGNAL_TMS;

  if(ESP32_TCK)
    output_signal_state |= SIGNAL_TCK;

  if(!nESP32_EN)
    output_signal_state |= SIGNAL_RESET;

    return ((uint16_t)input_signal_state << 8) | ((uint16_t)output_signal_state);
}

/**
 * Set state of JTAG output signals.
 *
 * @param low signals which should be de-asserted.
 * @param high signals which should be asserted.
 */
void jtag_set_signals(uint8_t low, uint8_t high)
{
  if(low & SIGNAL_TDI)    ESP32_TDI = 0;
  if(low & SIGNAL_TMS)    ESP32_TMS = 0;
  if(low & SIGNAL_TCK)    ESP32_TCK = 0;
  if(low & SIGNAL_RESET)  
  {
    ESP32_IO0 = 1; /* when in jtag mode, we want to debug the firmware, not the bootloader */
    nESP32_EN = 0;
  }

  if(high & SIGNAL_TDI)   ESP32_TDI = 1;
  if(high & SIGNAL_TMS)   ESP32_TMS = 1;
  if(high & SIGNAL_TCK)   ESP32_TCK = 1;
  if(high & SIGNAL_RESET) nESP32_EN = 1;
}

