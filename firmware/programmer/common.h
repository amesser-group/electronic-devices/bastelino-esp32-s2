/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BASTELINO_PROGRAMMER_H_
#define BASTELINO_PROGRAMMER_H_

#include <compiler.h>
#include <stdint.h>
#include <string.h>

#include "ch554.h"
#include "ch554_usb.h"

#define USB_DESCR_WORD(x) (uint8_t)(x & 0xFF), (uint8_t)((x >> 8) & 0xFF)
#define USB_DWORD(x)      (uint8_t)(x & 0xFF), (uint8_t)((x >> 8) & 0xFF), (uint8_t)((x >> 16) & 0xFF), (uint8_t)((x >> 24) & 0xFF)


struct control_transfer_state
{
  union {
    __code  uint8_t *const_buffer;
    __xdata uint8_t *setup_out_buffer;
  };

  int_least16_t      len;

  uint_least8_t      request;

  uint_least8_t      request_type;
};

__xdata __at (0x0000) uint8_t usb_ep2_out_buf[2*MAX_PACKET_SIZE];
__xdata __at (0x0080) uint8_t usb_ep2_in_buf[2*MAX_PACKET_SIZE];

/* usb endpoint buffers for jtag */
__xdata __at (0x0100) uint8_t usb_ep_out_jtag[MAX_PACKET_SIZE];
__xdata __at (0x0140) uint8_t usb_ep_in_jtag[MAX_PACKET_SIZE];

__xdata __at (0x0180) uint8_t usb_ep0_buffer[DEFAULT_ENDP0_SIZE];
__xdata __at (0x01C0) uint8_t usb_ep1_buffer[DEFAULT_ENDP1_SIZE];

/** amount of bytes in the usb buffer */
__idata extern uint8_t usb_ep_out_jtag_bytecount;

__idata extern uint8_t usb_config_no;

__idata extern volatile uint8_t System_ticks;
__idata extern uint16_t         System_timer_reload_1ms_div12;
__bit   extern                  System_booted;


__idata extern uint8_t UsbCdc_ep_out_datalen[2];


/* we must declare all isr routines such that they are visible in main.c */
void DeviceInterrupt(void) __interrupt (INT_NO_USB);
void Uart0Isr(void) __interrupt (INT_NO_UART0);
void Timer0Isr(void) __interrupt (INT_NO_TMR0);
void JTAG_TimerIsr(void) __interrupt (INT_NO_TMR1);
void System_PWMIsr(void) __interrupt (INT_NO_PWMX);

void	  System_UpdateClock();
uint8_t System_SetBaudrate(uint32_t baudrate);

void    System_ReportActivity();

void usb_setup_get_descriptor_transaction(__idata struct control_transfer_state* transaction);

void usb_cdc_out_transfer();
void usb_cdc_in_transfer();
void UsbCdc_Poll();

void UsbCdc_HandleControlSetup(__idata struct control_transfer_state *transaction);
void UsbCdc_HandleControlOut(__idata struct control_transfer_state *transaction);
void UsbCdc_SysTick();

void UsbCdc_SendMsg(__code const char* msg, uint16_t val);
void UsbCdc_SendHex(__code const char* msg, __xdata const uint8_t *data, uint8_t len);

void UsbEspJtag_HandleControlSetup(__idata struct control_transfer_state *transaction);
void UsbEspJtag_HandleInTransfer();
void UsbEspJtag_HandleOutTransfer();
void UsbEspJtag_Poll();

#endif