/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  bastelino on-board programmer is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"

#define CDC_COMMUNICATIONS_INTERFACE_CLASS    0x02
#define CDC_COMMUNICATIONS_SUBCLASS_ACM       0x02

#define CDC_COMMUNICATIONS_PROTOCOL_V250      0x01
#define CDC_DATA_INTERFACE_CLASS              0x0A

#define USB_DESCR_TYP_IAD       0x0B

/* The following USB Vendor/Pid is taken from OpenOCD source
*  src/jtag/drivers/ulink.c 
 *
 * WARNING: Do not sell or redistribute any device using this firmware
 *          The VID is owned by KEIL.
 * 
 * We currently re-use the vid in order to avoid adapting the 
 * openocd ulink driver. TODO: Use own VID/PID combination */


#define USB_VID 0xC251
#define USB_PID 0x2710

#define USB_STRING_DSC_OPENULINK    1
#define USB_STRING_DSC_MANUFACTURER 2
#define USB_STRING_DSC_PRODUCT      3
#define USB_STRING_DSC_SERIAL       4

__code uint8_t DevDesc[] = 
{
    0x12,USB_DESCR_TYP_DEVICE,
    0x10,0x01, /* USB1.1*/
    0x00,0x00,0x00,
    DEFAULT_ENDP0_SIZE,
    USB_DESCR_WORD(USB_VID), /* vendor id */
    USB_DESCR_WORD(USB_PID), /* product id */

    0x00,0x02, /* device release */
    USB_STRING_DSC_MANUFACTURER,
    USB_STRING_DSC_PRODUCT,
    USB_STRING_DSC_SERIAL,

    0x01, /* number of configurations */
};


__code uint8_t CfgDesc[] ={
    0x09, USB_DESCR_TYP_CONFIG,
    USB_DESCR_WORD(98), /* total descriptor length */
    0x03,    /* number of interfaces */
    0x01,    /* configuration value */ 
    0x00,    /* configuration string index */
    0x80,    /* attributes */
    500 / 2, /* max power */

    /*******************************************************************
     * OpenULINK JTAG uses interface 0
     * See OpenOCD source src/jtag/drivers/ulink.c for more details
     *******************************************************************/

    0x09,USB_DESCR_TYP_INTERF,
    0x00, /* interface number */
    0x00, /* alternate setting */
    0x02, /* number of endpoints */ 
    0xFF, 0xFF, 0x01, /* vendor specific device class */
    USB_STRING_DSC_PRODUCT, /* interface string index */

    0x07,USB_DESCR_TYP_ENDP,
    0x02, /* endpoint address 0x3, direction out */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */

    0x07,USB_DESCR_TYP_ENDP,
    0x82, /* endpoint address 0x3, direction in */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */

    /*******************************************************************
     * CDC
     *******************************************************************/

    /* interface association for cdc */
    0x08,USB_DESCR_TYP_IAD,
    0x01, /* first interface number */
    0x02, /* interface count */
    CDC_COMMUNICATIONS_INTERFACE_CLASS,CDC_COMMUNICATIONS_SUBCLASS_ACM,CDC_COMMUNICATIONS_PROTOCOL_V250,
    0x00,

    0x09,USB_DESCR_TYP_INTERF,
    0x01, /* interface number */
    0x00, /* alternate setting */
    0x01, /* number of endpoints */ 
    CDC_COMMUNICATIONS_INTERFACE_CLASS, CDC_COMMUNICATIONS_SUBCLASS_ACM, CDC_COMMUNICATIONS_PROTOCOL_V250, /* cdc device class */
    USB_STRING_DSC_PRODUCT, /* interface string index */

    
    0x05,USB_DESCR_TYP_CS_INTF,0x00,0x10,0x01,
    0x05,0x24,0x01,0x00,0x00,
    0x04,0x24,0x02,0x02,
    /* CDC Union */
    0x05,0x24,0x06,0x01,0x02,

    0x07,USB_DESCR_TYP_ENDP,
    0x81,               /* endpoint address 0x1, direction in */
    0x03,               /* interrupt, no sync, data endppint */
    USB_DESCR_WORD(8),  /* max packet size */
    0xFF,               /* poll interval */


    0x09,USB_DESCR_TYP_INTERF,
    0x02, /* interface number */ 
    0x00, /* alternate setting */
    0x02, /* number of endpoints */
    CDC_DATA_INTERFACE_CLASS, 0x00, 0x00,
    USB_STRING_DSC_PRODUCT, /* interface string index */

    0x07,USB_DESCR_TYP_ENDP,
    0x03, /* endpoint address 0x3, direction out */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */

    0x07,USB_DESCR_TYP_ENDP,
    0x83, /* endpoint address 0x3, direction in */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */
};

/* supported languages */
const unsigned char  __code usb_stringdescr_lang[] = {
  0x04, USB_DESCR_TYP_STRING,
  USB_DESCR_WORD(0x0409), /* us english */
};

const unsigned char  __code usb_stringdescr_serial[]       = "\x0A\x03" "n\x00" "o\x00" "n\x00" "e\x00";
const unsigned char  __code usb_stringdescr_product[]      = "\x26\x03" "B\x00" "a\x00" "s\x00" "t\x00" "e\x00" "l\x00" "i\x00" "n\x00" "o\x00" " \x00" "E\x00" "S\x00" "P\x00" "3\x00" "2\x00" "-\x00" "S\x00" "2\x00";
const unsigned char  __code usb_stringdescr_manufacturer[] = "\x14\x03" "B\x00" "a\x00" "s\x00" "t\x00" "e\x00" "l\x00" "m\x00" "a\x00" "p\x00";
const unsigned char  __code usb_stringdescr_ulink[]        = "\x14\x03" "O\x00" "p\x00" "e\x00" "n\x00" "U\x00" "L\x00" "I\x00" "N\x00" "K\x00";

void
usb_setup_get_descriptor_transaction(__idata struct control_transfer_state* transaction)
{
  __xdata USB_SETUP_REQ* setup_req = (__xdata USB_SETUP_REQ*)transaction->setup_out_buffer;
  int16_t setup_req_len;

  if(setup_req->wLengthH >= 0x80)
      setup_req_len = INT16_MAX;
  else
      setup_req_len = ((int16_t)setup_req->wLengthH<<8) | (setup_req->wLengthL);

  transaction->len = -1;

  switch(setup_req->wValueH)
  {
  case 1:
    transaction->const_buffer = DevDesc;
    transaction->len   = sizeof(DevDesc);
    break;
  case 2:
    transaction->const_buffer = CfgDesc;
    transaction->len   = sizeof(CfgDesc);
    break;
  case 3:
    switch(setup_req->wValueL)
    {
    case 0:
      transaction->const_buffer = usb_stringdescr_lang;
      transaction->len = transaction->const_buffer[0];
      break;
    case USB_STRING_DSC_OPENULINK:
      transaction->const_buffer = usb_stringdescr_ulink;
      transaction->len = transaction->const_buffer[0];
      break;
    case USB_STRING_DSC_MANUFACTURER:
      transaction->const_buffer = usb_stringdescr_manufacturer;
      transaction->len = transaction->const_buffer[0];
      break;
    case USB_STRING_DSC_PRODUCT:
      transaction->const_buffer = usb_stringdescr_product;
      transaction->len = transaction->const_buffer[0];
      break;
    case USB_STRING_DSC_SERIAL:
      transaction->const_buffer = usb_stringdescr_serial;
      transaction->len = transaction->const_buffer[0];
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }

  if (transaction->len > 0 && transaction->len <= 8)
    memcpy(setup_req, transaction->const_buffer, transaction->len);

  if (transaction->len > setup_req_len)
      transaction->len = setup_req_len;
}

