############################
Bastelino onboard programmer
############################

Peripheral assignment
=====================

* Timer 0: System Tick
* Timer 1: JTAG Timing
* Timer 2: UART Baud Rate generator


Current consumption
===================

Tested with musicbox firmware.

- rev aed950e: CH552 Fsys fixed at 24MHz: 35mA@5V
- rev b19b10b: CH552 Fsys dynamic set, 6MHz minimum: 29mA@5V
- rev 0d42062: CH552 Enter standby if USB not used, Power Led Duty reduced: 27mA@5V
- rev f9c4c9c: Fix UART TX, Use ESP32S2 Firmware with light slee: 10mA@5V

Copyright & License
===================

Parts of the source of the OpenULINK firmware provided
by openocd have been included & modified in this work.
For this original source the copyright is:

   Copyright (C) 2011 Martin Schmoelzer <martin.schmoelzer@student.tuwien.ac.at>

modifications & all other source copyright is:

   Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>

The Bastelino programmer firmware is distributed under the terms of 
the GNU General Public License version 3
