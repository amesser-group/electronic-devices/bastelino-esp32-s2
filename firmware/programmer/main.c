/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  bastelino on-board programmer firmware is distributed in the 
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 *  even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with bastelino on-board programmer firmware.  
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "io.h"
#include "openulink_protocol.h"

#include <stdio.h>
#include <string.h>

#include "ch554_usb.h"
 
static void System_ConfigureUSB()
{
  USB_CTRL   = 0x00;
  USB_CTRL   = bUC_SYS_CTRL0 | bUC_INT_BUSY | bUC_DMA_EN;
  USB_DEV_AD = 0x00;

  UDEV_CTRL  = bUD_PD_DIS | bUD_PORT_EN;

  UEP0_DMA = (uint16_t) usb_ep0_buffer;
  UEP1_DMA = (uint16_t) usb_ep1_buffer;
  UEP2_DMA = (uint16_t) usb_ep_out_jtag;
  UEP3_DMA = (uint16_t) usb_ep2_out_buf;

  UEP2_3_MOD = bUEP3_RX_EN | bUEP3_TX_EN | bUEP3_BUF_MOD |
               bUEP2_RX_EN | bUEP2_TX_EN ;

  UEP4_1_MOD = bUEP1_TX_EN;                                    

  UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
  UEP1_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK;           
  UEP2_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;

  /* On EP3 we don't use auto Toggle since we want to have proper control
   * of DMA Buffer use */
  UEP3_CTRL = /* bUEP_AUTO_TOG | */ UEP_T_RES_NAK | UEP_R_RES_ACK;

  UEP0_T_LEN = 0;
  UEP1_T_LEN = 0;
  UEP2_T_LEN = 0;
  UEP3_T_LEN = 0;

  /* configure usb interrupts */
  USB_INT_EN  = bUIE_SUSPEND | bUIE_TRANSFER | bUIE_BUS_RST;
  USB_INT_FG  = 0x1F;
  IE_USB      = 1;
}

main()
{
  System_UpdateClock();

  /* push pull outputs */
  P1_MOD_OC = ~( (0x01 << 1) |
                 (0x01 << 4) | 
                 (0x01 << 5) |
                 (0x01 << 7));

  P3_MOD_OC = ~( (0x01 << 1) |
                 (0x01 << 3));

  /* timer 0/1 16 bit mode, */
  TMOD   = bT0_M0 | bT1_M0;

  /* timer0/1 clock with fsys/12 
   * timer2 clock with fsys (used for uart baud rate ) */
  T2MOD  = bTMR_CLK | bT2_CLK;

  /* setup pwm for led */
  PWM_CTRL  = bPWM2_POLAR | bPWM2_OUT_EN | bPWM_IE_END | bPWM_IF_END;
  IE_PWMX   = 1;

  TR0 = 1;

  /* enable interrupts for all timers */
  ET0 = 1;
  ET1 = 1;

  /* enable uart interrupt */
  ES = 1;

  /* Configure wakeup sources */
  SAFE_MOD  = 0x55;
  SAFE_MOD  = 0xAA;
  WAKE_CTRL = bWAK_BY_USB;

  System_ConfigureUSB();
  EA = 1;

  while(System_ticks < 5);

  /* boot ESP32 */
  ESP32_IO0 = 1;

  while(System_ticks < 20);
  /* release esp32 from reset */
  nESP32_EN = 0;

  /* connect to USB */
  USB_CTRL |= bUC_DEV_PU_EN;

  System_booted = 1;

  while(1)
  {
    if(usb_config_no != 0)
    {
      UsbCdc_Poll();
      PollJTAG();
    }
  }
}
