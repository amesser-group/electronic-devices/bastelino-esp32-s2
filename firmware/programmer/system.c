/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  bastelino on-board programmer firmware is distributed in the 
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 *  even the implied warranty of MERCHANTABILITY or 
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with bastelino on-board programmer firmware.  
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "openulink_protocol.h"

__idata static uint8_t Clock_cksel_uart       = 0;
__idata uint16_t       System_timer_reload_1ms_div12 = 0;
__bit                  System_booted = 0;

__idata volatile uint8_t System_ticks;
__idata static volatile uint8_t System_ticks_led_act;

__code const uint16_t Clock_reload_table_1ms_div12[8] =
{
  (uint16_t)(0 -   187500 / 12 / 1000),
  (uint16_t)(0 -   750000 / 12 / 1000),
  (uint16_t)(0 -  3000000 / 12 / 1000),
  (uint16_t)(0 -  6000000 / 12 / 1000),
  (uint16_t)(0 - 12000000 / 12 / 1000),
  (uint16_t)(0 - 16000000 / 12 / 1000),
  (uint16_t)(0 - 24000000 / 12 / 1000),
  (uint16_t)(0 - 30000000 / 12 / 1000)
};

#pragma save
#pragma nooverlay

void	
System_UpdateClock()
{
  uint8_t clock_cfg = Clock_cksel_uart;

  if(clock_cfg == 0)
  {
    if(JTAG_active)
      clock_cfg = SYS_CK_SEL_24MHZ;
    else
      clock_cfg = SYS_CK_SEL_6MHZ; /* slowest speed for working usb is 6mhz */
  }
  
  SAFE_MOD  = 0x55;
  SAFE_MOD  = 0xAA;
  CLOCK_CFG = (CLOCK_CFG & ~MASK_SYS_CK_SEL) | clock_cfg;
  SAFE_MOD  = 0x00;

  System_timer_reload_1ms_div12 = Clock_reload_table_1ms_div12[clock_cfg];
}


uint8_t
System_SetBaudrate(uint32_t baudrate)
{
  uint16_t reload;

  switch(baudrate)
  {
  case 115200: 
    Clock_cksel_uart = SYS_CK_SEL_24MHZ;   
    reload = (uint16_t)(0U - 24000000U/16U/115200U); 
    break;
  default:     
    Clock_cksel_uart = 0; 
    reload = 0; 
    break;
  }

  T2CON = 0;
  System_UpdateClock();

  /* Clock Uart0 by timer 2 */
  RCAP2 = reload;
  T2CON = bRCLK | bTCLK | bTR2;
  TR2   = 1;

  return (reload > 0) ? 1 : 0;
}

void
System_ReportActivity()
{
  System_ticks_led_act = 200;
}
#pragma restore

void 
Timer0Isr(void) __interrupt (INT_NO_TMR0)
{
  TH0 = System_timer_reload_1ms_div12 / 256;
  TL0 = System_timer_reload_1ms_div12 % 256;

  ++System_ticks;

  if(System_ticks_led_act > 0)
    --System_ticks_led_act;

  UsbCdc_SysTick();
}

void 
System_PWMIsr(void) __interrupt (INT_NO_PWMX)
{
  PWM_CTRL |= bPWM_IF_END;

  if(!System_booted)
  {
    PWM_DATA2 = 0x80;
  }
  else if(System_ticks_led_act > 0)
  { /* USB activity reported */
    PWM_DATA2 = 0x80;
  }
  else if( (UsbCdc_ep_out_datalen[0] != 0) ||
           (UsbCdc_ep_out_datalen[1] != 0) )
  { /* UART TX data pending */
    PWM_DATA2 = 0x80;    
  }
  else if ( (USB_MIS_ST & bUMS_SUSPEND ) ||
            (usb_config_no == 0) )
  { /* standby mode */
    PWM_DATA2 = 0x02;
    PCON |= PD; 
  }
  else
  { /* USB configured */
    PWM_DATA2 = 0x20;
  }
}


