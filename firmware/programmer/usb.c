/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of bastelino on-board programmer firmware.
 *
 *  bastelino on-board programmer firmware is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"

__idata uint8_t usb_ep_out_jtag_bytecount;

__idata static struct control_transfer_state ep0_transaction;
__idata static uint8_t usb_device_addr;
__idata        uint8_t usb_config_no;

#pragma save
#pragma nooverlay
static int16_t
transaction_get_next_data(struct control_transfer_state* t, __xdata void* buffer, int8_t len)
{
  if(t->len < 0)
    return t->len;

  if(t->len < len)
    len = t->len;

  memcpy(buffer, t->const_buffer, len);

  t->len           -= len;
  t->const_buffer  += len;

  return len;
}

/** get first data of control transfer 
 * 
 * if t has assigned length <= 8, assume data has been placed in ep0 buffer already 
 */
static int16_t
transaction_get_first_data(struct control_transfer_state* t, __xdata void* buffer, int8_t len)
{
  if(t->len <= 8)
  {
    len = t->len;
    t->len = 0; 

    return len;
  }
  else
  {
    return transaction_get_next_data(t, buffer, len);
  }
}

static void
Usb_HandleControlSetup(__idata struct control_transfer_state *transaction)
{
  const __xdata USB_SETUP_REQ* setup_req = (__xdata USB_SETUP_REQ*)(transaction->setup_out_buffer);

  switch(transaction->request)
  {
  case USB_GET_DESCRIPTOR:
    usb_setup_get_descriptor_transaction(transaction);
    break;
  case USB_SET_ADDRESS:
    usb_device_addr = setup_req->wValueL;
    break;
  case USB_GET_CONFIGURATION:
      transaction->setup_out_buffer[0] = usb_config_no;
      transaction->len = 1;
      break;
  case USB_SET_CONFIGURATION:
      usb_config_no = setup_req->wValueL;
      break;
  case USB_GET_INTERFACE:
      break;
  case USB_CLEAR_FEATURE:
      if( ( transaction->request_type & 0x1F ) == USB_REQ_RECIP_DEVICE )
      {
          ep0_transaction.len = - 1;
      }
      else if ( ( transaction->request_type & USB_REQ_RECIP_MASK ) == USB_REQ_RECIP_ENDP )
      {
          switch( setup_req->wIndexL )
          {
          case 0x83:
              UEP3_CTRL = UEP3_CTRL & ~ ( bUEP_T_TOG | MASK_UEP_T_RES ) | UEP_T_RES_NAK;
              break;
          case 0x03:
              UEP3_CTRL = UEP3_CTRL & ~ ( bUEP_R_TOG | MASK_UEP_R_RES ) | UEP_R_RES_ACK;
              break;
          case 0x82:
              UEP2_CTRL = UEP2_CTRL & ~ ( bUEP_T_TOG | MASK_UEP_T_RES ) | UEP_T_RES_NAK;
              break;
          case 0x02:
              UEP2_CTRL = UEP2_CTRL & ~ ( bUEP_R_TOG | MASK_UEP_R_RES ) | UEP_R_RES_ACK;
              break;
          case 0x81:
              UEP1_CTRL = UEP1_CTRL & ~ ( bUEP_T_TOG | MASK_UEP_T_RES ) | UEP_T_RES_NAK;
              break;
          case 0x01:
              UEP1_CTRL = UEP1_CTRL & ~ ( bUEP_R_TOG | MASK_UEP_R_RES ) | UEP_R_RES_ACK;
              break;
          default:
              ep0_transaction.len = - 1;
              break;
          }
      }
      else
      {
          ep0_transaction.len = - 1;
      }
      break;
  case USB_SET_FEATURE:
      if( ( transaction->request_type & 0x1F ) == USB_REQ_RECIP_DEVICE )
      {
        ep0_transaction.len = - 1;
      }
      else if( ( transaction->request_type & 0x1F ) == USB_REQ_RECIP_ENDP )
      {
        const uint16_t wValue = ( ( uint16_t )setup_req->wValueH << 8 ) | setup_req->wValueL;
        const uint16_t wIndex = ( ( uint16_t )setup_req->wIndexH << 8 ) | setup_req->wIndexL;

          if( wValue == 0x00 )
          {
              switch( wIndex )
              {
              case 0x83:
                  UEP3_CTRL = UEP3_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
                  break;
              case 0x03:
                  UEP3_CTRL = UEP3_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
                  break;
              case 0x82:
                  UEP2_CTRL = UEP2_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
                  break;
              case 0x02:
                  UEP2_CTRL = UEP2_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
                  break;
              case 0x81:
                  UEP1_CTRL = UEP1_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
                  break;
              case 0x01:
                UEP1_CTRL = UEP1_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
              default:
                  ep0_transaction.len = -1;
                  break;
              }
          }
          else
          {
              ep0_transaction.len =  -1;
          }
      }
      else
      {
          ep0_transaction.len = -1;
      }
      break;
  case USB_GET_STATUS:
      ep0_transaction.setup_out_buffer[0] = 0;
      ep0_transaction.setup_out_buffer[1] = 0;
      ep0_transaction.len = 2;
      break;
  default:
      ep0_transaction.len = -1;
      break;
  }
}
#pragma restore

void DeviceInterrupt(void) __interrupt (INT_NO_USB)
{
    int8_t len;
    if(UIF_TRANSFER)
    {
        switch (USB_INT_ST & (MASK_UIS_TOKEN | MASK_UIS_ENDP))
        {
        /* cdc interrupt */
        case UIS_TOKEN_IN | 1:
            UEP1_T_LEN = 0;
            UEP1_CTRL = UEP1_CTRL & ~ MASK_UEP_T_RES | UEP_T_RES_NAK;
            break;
        /* ESP JTAG */
        case UIS_TOKEN_IN | 2:
            /* stop further in-transmissions */
            UEP2_T_LEN  = 0;
            UEP2_CTRL  |= UEP_T_RES_NAK;
            break;
        case UIS_TOKEN_OUT | 2:
            if ( U_TOG_OK )
            {
              /* stop further out processing */
              usb_ep_out_jtag_bytecount = USB_RX_LEN;
              UEP2_CTRL |= UEP_R_RES_NAK; 
            }
            break;
        /* CDC data communications */
        case UIS_TOKEN_IN | 3:
            usb_cdc_in_transfer();
            break;
        case UIS_TOKEN_OUT | 3:
            if ( U_TOG_OK )
                usb_cdc_out_transfer();
            break;
        case UIS_TOKEN_SETUP | 0:
            len = USB_RX_LEN;

            if(len == (sizeof(USB_SETUP_REQ)))
            {
                const __xdata USB_SETUP_REQ* UsbSetupBuf = (USB_SETUP_REQ*)usb_ep0_buffer;

                ep0_transaction.len              = 0;
                ep0_transaction.setup_out_buffer = usb_ep0_buffer;
                ep0_transaction.request          = UsbSetupBuf->bRequest;
                ep0_transaction.request_type     = UsbSetupBuf->bRequestType;

                if ( ( ep0_transaction.request_type & USB_REQ_TYP_MASK ) == USB_REQ_TYP_STANDARD )
                {
                  Usb_HandleControlSetup(&ep0_transaction);
                }
                else
                {
                  if((ep0_transaction.request_type & USB_REQ_TYP_MASK) == USB_REQ_TYP_CLASS)
                    UsbCdc_HandleControlSetup(&ep0_transaction);
                  else
                    ep0_transaction.len = -1;
                }
            }
            else
            {
                len = -1;
            }

            len = transaction_get_first_data(&ep0_transaction, usb_ep0_buffer, DEFAULT_ENDP0_SIZE);

            if(len < 0)
            {
                ep0_transaction.request = 0xFF;
                UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_STALL | UEP_T_RES_STALL;
            }
            else if(len <= DEFAULT_ENDP0_SIZE)
            {
                UEP0_T_LEN = len;
                UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_ACK | UEP_T_RES_ACK;
            }
            else
            {
                UEP0_T_LEN = 0;
                UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_ACK | UEP_T_RES_ACK;
            }
            break;
        case UIS_TOKEN_IN | 0:                                                      //endpoint0 IN
            switch(ep0_transaction.request)
            {
            case USB_GET_DESCRIPTOR:
                UEP0_T_LEN = transaction_get_next_data(&ep0_transaction, usb_ep0_buffer, DEFAULT_ENDP0_SIZE);
                UEP0_CTRL ^= bUEP_T_TOG;                                             //同步标志位翻转
                break;
            case USB_SET_ADDRESS:
                USB_DEV_AD = USB_DEV_AD & bUDA_GP_BIT | usb_device_addr;
                UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
                break;
            default:
                UEP0_T_LEN = 0;
                UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
                break;
            }
            break;
        case UIS_TOKEN_OUT | 0:
            ep0_transaction.len              = USB_RX_LEN;
            ep0_transaction.setup_out_buffer = usb_ep0_buffer;

            if ( ( ep0_transaction.request_type & USB_REQ_TYP_MASK ) != USB_REQ_TYP_STANDARD )
            {
              UsbCdc_HandleControlOut(&ep0_transaction);    
            }
            else
            {
              ep0_transaction.len = -1;
            }
            
            if(ep0_transaction.len >= 0)
            {
                UEP0_T_LEN = 0;
                UEP0_CTRL |= UEP_R_RES_ACK | UEP_T_RES_ACK;
            }
            else
            {
                UEP0_T_LEN = 0;
                UEP0_CTRL |= UEP_R_RES_ACK | UEP_T_RES_NAK;
            }
            break;

        default:
            break;
        }

        UIF_TRANSFER = 0;
    }

    if(UIF_BUS_RST)
    { /* Bus reset */
        UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
        UEP1_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK;
        UEP2_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;
        UEP3_CTRL = /* bUEP_AUTO_TOG | */ UEP_T_RES_NAK | UEP_R_RES_ACK;

        USB_DEV_AD = 0x00;
        UIF_SUSPEND   = 0;
        UIF_TRANSFER  = 0;
        UIF_BUS_RST   = 0;
        usb_config_no = 0;
    }

    if (UIF_SUSPEND)
        UIF_SUSPEND = 0;

  System_ReportActivity();
}
